﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Media3D;
using System.Windows.Threading;
using System.IO;

namespace MagicCube
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region 私有属性

        private MagicCubeModel magicCubeFront = new MagicCubeModel(); //魔方对象
        private MagicCubeModel magicCubeBack = new MagicCubeModel(); //魔方对象
        private bool isRightButtomDown = false; // 鼠标右键是否按下
        private Point beforeMove; //鼠标拖动前坐标
        private Point afterMove; //鼠标拖动后坐标
        private Random random = new Random(); //随机属性
        private bool btnUpsetDown = false; //乱序按钮是否按下

        #endregion

        #region 初始化函数

        public MainWindow()
        {
            InitializeComponent();
            Show_Guide();
            Create_MagicCube();
        }
        //将模型加入3D场景
        private void Create_MagicCube()
        {
            viewFront.Children.Add(magicCubeFront.CubeModel3D);
            viewBack.Children.Add(magicCubeBack.CubeModel3D);
        }
        //转动魔方
        private void Rotate_MagicCube(Spindle spindle, bool isClockwise, int groupNumber)
        {
            if(magicCubeFront.RotateCubeGroup(spindle, isClockwise, groupNumber))
            {
                if(magicCubeBack.RotateCubeGroup(spindle, isClockwise, groupNumber)) return;
            }
        }
        //显示鼠标操作指南
        private void Show_Guide()
        { 
            txtGuide.Text = "1. 按住鼠标左键上下左右划动并松手：可上下左右转动整个魔方，松手后停在相邻面。\r\n\r\n2. 按住鼠标右键上下左右划动并松手：可上下左右转动整个魔方，松手后返回当前面。";
        }

        #endregion

        #region 响应鼠标

        private void viewGrid_MouseMove(object sender, MouseEventArgs e)
        {
            int x = (int)e.GetPosition(viewGrid).X;
            int y = (int)e.GetPosition(viewGrid).Y;
            txtCoord.Text = x + " , " + y;
            if (isRightButtomDown)
            {
                afterMove = e.GetPosition(viewGrid);
                double mx = afterMove.X - beforeMove.X;
                axisY.Angle += -mx;
                double my = afterMove.Y - beforeMove.Y;
                axisX.Angle += -my;
                beforeMove = afterMove;
            }
        }

        private void viewGrid_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            beforeMove = e.GetPosition(viewGrid);
            isRightButtomDown = true;
        }

        private void viewGrid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            axisX.Angle = 0;
            axisY.Angle = 0;
            isRightButtomDown = false;
        }

        private void viewGrid_MouseLeave(object sender, MouseEventArgs e)
        {
            axisX.Angle = 0;
            axisY.Angle = 0;
            isRightButtomDown = false;
        }

        private void viewGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            beforeMove = e.GetPosition(viewGrid);
        }

        private void viewGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            afterMove = e.GetPosition(viewGrid);
            double mx = afterMove.X - beforeMove.X;
            double my = afterMove.Y - beforeMove.Y;
            if ((my > -20 && my < 20))
            {
                if (mx > 20) Rotate_MagicCube(Spindle.Y, false, 0);
                else if (mx < -20) Rotate_MagicCube(Spindle.Y, true, 0);
            }
            else if (mx > -20 && mx < 20)
            {
                if (my > 20) Rotate_MagicCube(Spindle.X, false, 0);
                else if (my < -20) Rotate_MagicCube(Spindle.X, true, 0);
            }
        }

        #endregion

        #region 响应按钮

        private void btnTopLeft_Click(object sender, RoutedEventArgs e)
        {
            Rotate_MagicCube(Spindle.Y, true, 4);
        }

        private void btnTopRight_Click(object sender, RoutedEventArgs e)
        {
            Rotate_MagicCube(Spindle.Y, false, 4);
        }

        private void btnDownLeft_Click(object sender, RoutedEventArgs e)
        {
            Rotate_MagicCube(Spindle.Y, true, 6);
        }

        private void btnDownRight_Click(object sender, RoutedEventArgs e)
        {
            Rotate_MagicCube(Spindle.Y, false, 6);
        }

        private void btnLeftUp_Click(object sender, RoutedEventArgs e)
        {
            Rotate_MagicCube(Spindle.X, true, 9);
        }

        private void btnLeftDown_Click(object sender, RoutedEventArgs e)
        {
            Rotate_MagicCube(Spindle.X, false, 9);
        }

        private void btnRightUp_Click(object sender, RoutedEventArgs e)
        {
            Rotate_MagicCube(Spindle.X, true, 7);
        }

        private void btnRightDown_Click(object sender, RoutedEventArgs e)
        {
            Rotate_MagicCube(Spindle.X, false, 7);
        }

        private void btnCenterLeft_Click(object sender, RoutedEventArgs e)
        {
            Rotate_MagicCube(Spindle.Y, true, 5);
        }

        private void btnCenterRight_Click(object sender, RoutedEventArgs e)
        {
            Rotate_MagicCube(Spindle.Y, false, 5);
        }

        private void btnAntiClockwise_Click(object sender, RoutedEventArgs e)
        {
            Rotate_MagicCube(Spindle.Z, false, 1);
        }

        private void btnClockwise_Click(object sender, RoutedEventArgs e)
        {
            Rotate_MagicCube(Spindle.Z, true, 1);
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            magicCubeFront.Create_CubeModel();
            magicCubeBack.Create_CubeModel();
        }

        private void btnUpset_Click(object sender, RoutedEventArgs e)
        {
            if (btnUpsetDown == false)
            {
                btnUpsetDown = true;
                Spindle[] spi = new Spindle[] { Spindle.X, Spindle.Y, Spindle.Z };
                bool[] isClo = new bool[] { true, false };
                int count = random.Next(36, 73);
                for (int i = 0; i < count; i++)
                {
                    Spindle spindle = spi[random.Next(0, 3)];
                    bool isClockwise = isClo[random.Next(0, 2)];
                    int groupNumber = random.Next(1, 10);
                    magicCubeFront.ChangeCubeGroup(spindle, isClockwise, groupNumber);
                    magicCubeBack.ChangeCubeGroup(spindle, isClockwise, groupNumber);
                }
                btnUpsetDown = false;
            }
        }

        #endregion
    }
}